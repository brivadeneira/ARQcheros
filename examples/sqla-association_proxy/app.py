from flask import Flask
from flask import Flask, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy
import enum
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship, backref
import flask_admin as admin
from flask_admin.contrib import sqla

# Create application
app = Flask(__name__)

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create in-memory database
app.config['SQLALCHEMY_DATABASE_URI'] =  app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
app.config['SQLALCHEMY_ECHO'] = True
db = SQLAlchemy(app)


# Flask views
#@app.route('/')
#def index():
     #generate_img("arqcheros"); #save inside static folder
     #return '<b>¡BIENVENIDO/A A ARQcheros!</b> <p><a href="/admin/">Click para comenzar</a></p> <img src=' + url_for('static',filename='test.jpg') + '>'
@app.route("/", methods=["GET"])
def index():
    return render_template('index.html')

##### ENUM #####
Roca = enum.Enum("Roca", [
    ('Roca no identificada', 'roca_no_identificada'),
    ("Arenisca", "arenisca"),
    ("Caliza silicificada", "caliza_silicificada"),
    ("Cuarcita de grano medio o grueso", "cuarcita_grano_medio_grueso"),
    ("Cuarzo cristalino", "cuarzo_cristalino"),
    ("Cuarzo lácteo", "cuarzo_lacteo"),
    ("Granito", "granito"),
    ("Ignimbrita", "ignimbrita"),
    ("Metacuarcita(grano fino)", "metacuarcita"),
    ("Metamórfica no identificada", "metaforica_no_id "),
    ("Obsidiana gris/negra, homogénea,manchada o bandeada", "obsidiana"),
    ("Sílices diversos(jaspes, calcedonias coloreadas)", "silices_diversos"),
    ("Vulcanitas ácidas, claras o coloreadas(riolitas, dacitas u otras) de grano fino", "vulcanitas_ac_claras"),
    ("Vulcanitas básicas grises o negras de grano fino", "vulcanitas_basicas"),
    ("Vulcanitas grano mediano a grueso", "vulcanitas_grano_mediano"),
    ("Xilópalos (vetas visibles)", "xilopalos"),
    ("Otros vidrios volcánicos claros o coloreados (no grises o negros)", "otros_vidrios")
    ])

Estado = enum.Enum("Estado", [
    ("Entera o completa", "entera"),
    ("Fracturada", "fracturada")
])

Tipo = enum.Enum("Tipo", [
    ("Simple", "simple"),
    ("Compuesta", "compuesta")
])

Eje = enum.Enum("Eje", [
    ("Eje técnico o de lascado", "eje_tecnico"),
    ("Eje morfológico", "eje_morfologico")
])

Clase_Art = enum.Enum("Clase Artefactual", [
    ("Artefactos con filos, puntas o superficies con rastros complementarios", "art_rastros"),
    ("Artefactos con filos, puntas y/o superficies formatizados", "art_sup"),
    ("Desechoss de talla", "Desechoss"),
    ("Nódulos con rastros complementarios", "nodulos_rastros"),
    ("Nódulos transportados sin rastros complementarios (rocas aloctonas)", "nodulos"),
    ("Núcleos", "nucleos")
])

Cant_Filos = enum.Enum("Cantidad de filos", [
    "0","1","2","3","4","5","6","7","8","9","20"
])

Cant_Puntas = enum.Enum("Cantidad de puntas formatizadas", [
    "0","1","2","3","4","5","6","7","8","9","20"
])

Clasificacion_Forma_Base = enum.Enum("Clasificación de forma base", [
    ("Forma Base no diferenciada (0Z)", "0z"),
    ("Hoja de aristas dobles o múltiples (3B)", "3b"),
    ("Hoja no diferenciada (3Z)", "3z"),
    ("Hoja reciclada (4C)", "4c"),
    ("Laja o nódulo tabular no rodado (1G)", "1g"),
    ("Lasca angular (2D)", "2d"),
    ("Lasca con dorso natural (2C)", "2c"),
    ("Lasca de arista simple o doble (2E)", "2e"),
    ("Lasca de flanco de nucleo (2H)", "2h"),
    ("Lasca de tableta de núcleo (2I)", "2i"),
    ("Lasca en cresta (2G)", "2g"),
    ("Lasca no diferenciada (2Z)", "2z"),
    ("Lasca plana (2F)", "2f"),
    ("Lasca primaria (2A)", "2a"),
    ("Lasca reciclada (4B)", "4b"),
    ("Lasca secundaria (2B)", "2b"),
    ("Lasca semi-tableta de núcleo (2J)", "2j"),
    ("Módulo no diferenciado (1Z)", "1z"),
    ("Nódulo o rodado a facetas naturales (1E)", "1e"),
    ("Nódulo tabular (rodado) (1F)", "1f"),
    ("Artefacto formatizado reciclado (4A)", "4a"),
    ("Artefacto reciclado, no diferenciado (4Z)", "4z"),
    ("Bloque no transportable, con/sin facetas (1H)", "1h"),
    ("Clasto (frag.anguloso natural) (1I)", "1i"),
    ("Concreción nodular(con restos de matriz) (1J)", "1j"),
    ("Núcleo reciclado 4D", "4d")
])

Cant_Cicatrices = enum.Enum("Cantidad de cicatrices de lascado", [
    "0","1","2","3","4","5","6","7","8","9","20"
])

Origen_extraccion = enum.Enum("Origen de la extracción", [
    ("Origen no diferenciado (9)", "9"),
    ("Adelgazamiento o reducción bifacial (2)", "2"),
    ("Reactivación de núcleos (4)", "4"),
    ("Reactivación de útiles o instrumento (3)", "3"),
    ("Talla de extracción (1)", "1")
])

Alteraciones = enum.Enum("Alteraciones", [
    ("Alteraciones no diferenciadas (Z0)", "az0"),
    ("Alteraciones térmicas múltiples (E4)", "ae4"),
    ("Alteraciones térmicas no diferenciadas (E0)", "ae0"),
    ("Alteracion térmica del color (E3)", "ae3"),
    ("Craqueleado (E2)", "ae2"),
    ("Desprendimientos cupulares u 'hoyuelos' (E1)", "ae1"),
    ("Lustre sin gradaciones diferenciadas (A0)", "aa0"),
    ("Patina grisacea o blanquecina 'en costra' (B2)", "ab2"),
    ("Patina rojiza o 'barniz del desierto' (B1)", "ab1"),
    ("Patina sin gradaciones diferenciadas (B0)", "ab0"),
    ("Rodamiento sin gradaciones (D0)", "ad0"),
    ("Ventifaccion sin gradaciones (C0)", "ac0")
])

Estado_Talon = enum.Enum("Estado del talón", [
    ("Entero", "entero"),
    ("Fracturado", "fracturado"),
    ("Rebajado", "rebajado"),
    ("Eli", "eli")
])

Superficie_Talon = enum.Enum("Superficie talón o plataforma", [
    ("Cortical (Ct)", "ct"),
    ("Liso-Cortical  (LiC)", "lic"),
    ("Liso (Li)", "li"),
    ("Facetado (Fc)", "fc"),
    ("Filiforme (Fi)", "fi"),
    ("Puntiforme (Pt)", "pt")
])

Clase_tecnica = enum.Enum("Clase técnica", [
    ("Artefacto con adelgazamiento bifacial", "acab"),
    ("Con reducción bifacial", "crb"),
    ("Bifacial marginales", "cm"),
    ("Adelgazamiento unifacial", "au"),
    ("Reducción unifacial", "ru"),
    ("Unifacial marginales", "um"),
    ("Talla de extracción sin formatización", "tesf"),
    ("Lito transportado (alóctono) no tallado", "ltnt")
])

Reduc_uni_sbordes = enum.Enum("Reducción unifacial sin bordes", ["Si", "No"])

Las_inv_lim = enum.Enum("Lascado inverso limitante", ["Si", "No"])

Proc_Tec = enum.Enum("Procedimientos técnicos formatizados", [
    ("No diferenciado", "ndif"),
    ("Por lascado simple de formatización", "plsdf"),
    ("Retalla (>7mm)", "rta"),
    ("Retoque (7mm a 2 mm)", "rto"),
    ("Microretoque (<2mm)", "mic"),
    ("Picado", "picado"),
    ("Alisado", "alisado"),
    ("Pulido", "pulido"),
    ("Talla bipolar", "tab"),
    ("Retalla bipolar", "retab"),
    ("Talla de extracción+retoque bipolar", "tex")
])

Forma_geo = enum.Enum("Forma geométrica", [
    #-----Filos-----
    ("Filo - Convexo atenuado o muy atenuado (A1)", "FA1"),
    ("Filo - Convexo medio (A2)", "FA2"),
    ("Filo - Convexo semicircular (A3)", "FA3"),
    ("Filo - Cóncavo atenuado o muy atenuado (B1)", "FB1"),
    ("Filo - Cóncavo medio (B2)", "FB2"),
    ("Filo - Cóncavo semicircular (B3)", "FB3"),
    ("Filo - Recto (C)", "FC"),
    ("Filo - Irregular - Combinados B/c ó A/C (D)", "FD"),
    #-----Puntas manuales (sección)-----
    ("Punta manual - Triédrica (E)", "PE"),
    ("Punta manual - Cuadrangular o trapezoidal (F)", "PF"),
    ("Punta manual - Plano-convexa (G)", "PG"),
    ("Punta manual -  Biconvexa (H)", "PH"),
    ("Punta manual - No diferenciada (Z)", "PZ"),
    #-----Limbos-----
    ("Limbo - Triangular corto convexilíneo (I)", "LI"),
    ("Limbo - Triangular largo convexilíneo (J)", "LJ"),
    ("Limbo - Triangular corto rectililneo (K)", "LK"),
    ("Limbo - Traingular largo rectilíneo (L)", "LL"),
    ("Limbo -Triangular corto concavilíneo (M)", "LM"),
    ("Limbo - Triangular largo concavilíneo (N)", "LN"),
    ("Limbo -Lanceolado (Ñ)", "LÑ"),
    ("Limbo -Lanceolado con bordes medios paralelos (O)", "LO"),
    ("Limbo - En mandorla (bipunta lanceolada) (P)", "LP"),
    ("Limbo - Lanceolada asimétrica o almendrada. (Q)", "LQ"),
    #-----Bordes de pedúnculos-----
    ("Borde - Paralelos o subparalelos (R)", "BR"),
    ("Borde - Divergentes hacia la base- (R)", "BD"),
    ("Borde - Convergentes, idem.- (S)", "BC"),
    ("Borde - Expandidos. (T)", "BT"),
    #-----Contornos-----
    ("Contorno - Oval (U)", "CU"),
    ("Contorno - Elípticos (V)", "CV"),
    ("Contorno - lanceolado (O)", "CO"),
    ("Contorno - Almendrado (Q)", "CQ"),
    ("Contorno - No diferenciada. (Z)", "CZ"),
    #-----Delineación aristas de piezas bifaciales-----
    ("Delineacion - Regular normal (X1)", "DX1"),
    ("Delineacion - Sinuosa regula (X2)", "DX2"),
    ("Delineacion - Sinuosa irregular (X3)", "DX3"),
    ("Delineacion - No diferenciada (Z)", "DZ")
])

Estado_bisel = enum.Enum("Estado de bisel", [
    ("No diferenciado (Z0)", "ezo"),
    ("Activo no astillado (A1)", "ea1"),
    ("Activo Astiladol (A2)", "ea2"),
    ("Embotado(+80°) (B1)", "eb1"),
    ("Embotado astillado (B2)", "eb2"),
    ("Con astillad.escalonadas (B3)", "eb3"),
    ("Recto (C)", "ec")
])

Mantenimiento = enum.Enum("Mantenimiento", ["Si", "No"])

Parte_pasiva = enum.Enum("Parte pasiva", [
    ("No diferenciado", "ppnd"),
    ("Dorso formatizado", "ppdf"),
    ("Dorso abatido (por lascado único)", "ppda"),
    ("Formatización sumaria de acomodación", "ppfsa"),
    ("Corteza reservada", "ppcr"),
    ("Plano de fractura utilizado", "ppcf"),
    ("Filo en ficha", "ppff"),
])

Forma_lascados = enum.Enum("Forma de lascados", [
    ("No diferenciado", "flnd"),
    ("Lasca simple", "flls"),
    ('Simple laminar "en golpe de buril"', "flgb"),
    ("Paralelo corto", "flpc"),
    ("Paralelo laminar", "flpl"),
    ("Escamoso", "fle"),
    ("Escamoso escalonado", "flee")
])

Sustancia = enum.Enum("Sustancia adherida", ["Si", "No"])

###################ENUM######################

class Observacion(db.Model):
    __tablename__ = 'Observacion'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column('Nombre o etiqueta de la observación', db.String(64))
    sitio = db.Column('Sitio o Localidad', db.String(64))
    sigla = db.Column('Sigla del sitio', db.String(64))
    capa = db.Column('Capa o nivel', db.String(64))
    coleccion = db.Column('Colección o año', db.String(64))
    operador = db.Column('Operador', db.String(64))
    fecha = db.Column('Fecha', db.DateTime)
    hoja = db.Column('Hoja N°', db.Integer)

    # Association proxy of "Observacion_Artefactoss" collection to "Artefactos" attribute - a list of Artefactoss objects.
    Artefactos = association_proxy('Artefactos', 'Artefactos')
    # Association proxy to association proxy - a list of Artefactoss strings.
    Artefactos_values = association_proxy('Artefactos', 'Artefactos_value')

    def __init__(self, name=None):
        self.name = name


class ObservacionArtefactos(db.Model):
    __tablename__ = 'Observacion_Artefactos'
    Observacion_id = db.Column(db.Integer, db.ForeignKey('Observacion.id'), primary_key=True)
    Artefactos_id = db.Column(db.Integer, db.ForeignKey('Artefactos.id'), primary_key=True)
    special_key = db.Column(db.String(50))

    # bidirectional attribute/collection of "Observacion"/"Observacion_Artefactoss"
    Observacion = relationship(Observacion, backref=backref("Artefactos", cascade="all, delete-orphan"))

    # reference to the "Artefactos" object
    Artefactos = relationship("Artefactos")
    # Reference to the "Artefactos" column inside the "Artefactos" object.
    Artefactos_value = association_proxy('id', 'Artefactos')

    def __init__(self, Artefactos=None, Observacion=None, special_key=None):
        self.Observacion = Observacion
        self.Artefactos = Artefactos
        self.special_key = special_key

class Artefactos(db.Model):
    __tablename__ = 'Artefactos'
    id = db.Column(db.Integer, primary_key=True)
    numero = db.Column('Número o sigla de la pieza', db.Integer)
    cuadro = db.Column('Cuadro o microsector', db.String(64))
    roca = db.Column('Roca o materia prima', db.Enum(Roca))
    estado = db.Column(db.Enum(Estado))
    tipo = db.Column(db.Enum(Tipo))
    eje = db.Column(db.Enum(Eje))
    clase_art = db.Column(db.Enum(Clase_Art))
    cant_filos = db.Column(db.Enum(Cant_Filos))
    cant_puntas = db.Column(db.Enum(Cant_Puntas))
    clasificacion_Forma_Base = db.Column(db.Enum(Clasificacion_Forma_Base))
    cant_Cicatrices = db.Column(db.Enum(Cant_Cicatrices))
    origen = db.Column(db.Enum(Origen_extraccion))
    alteraciones = db.Column(db.Enum(Alteraciones))
    estado_talon = db.Column(db.Enum(Estado_Talon))
    sup_talon = db.Column(db.Enum(Superficie_Talon))
    ancho_talon = db.Column(db.Integer)
    ancho_pieza = db.Column(db.Integer)
    long_pieza = db.Column(db.Integer)
    espesor_pieza = db.Column(db.Integer)
    peso_pieza = db.Column(db.Integer)
    tamanio_pieza = db.Column(db.Numeric)
    mod_ancho_largo_pieza = db.Column(db.Integer)
    mod_ancho_espesor_pieza = db.Column(db.Integer)
    clase_tecnica = db.Column(db.Enum(Clase_tecnica))
    reduc_uni_sbordes = db.Column(db.Enum(Reduc_uni_sbordes))
    las_inv_lim = db.Column(db.Enum(Las_inv_lim))
    proc_Tec = db.Column(db.Enum(Proc_Tec))
    forma_geo = db.Column(db.Enum(Forma_geo))
    angulo_bisel = db.Column(db.Integer)
    estado_bisel = db.Column(db.Enum(Estado_bisel))
    mantenimiento = db.Column(db.Enum(Mantenimiento))
    parte_pasiva =  db.Column(db.Enum(Parte_pasiva))
    forma_lascados = db.Column(db.Enum(Forma_lascados))
    sustancia = db.Column(db.Enum(Sustancia))
    ubicacion_sustancia = db.Column('Ubicacion de la sustancia', db.String(64))
    obs = db.Column('Observaciones', db.String(200))

    # Association proxy of "Artefactos_Detalless" collection to "Detalles" attribute - a list of Detalless objects.
    Detalles = association_proxy('Artefactos_Detalles', 'Detalles')
    # Association proxy to association proxy - a list of Detalless strings.
    Detalles_values = association_proxy('Artefactos_Detalles', 'Detalles_value')


    def __init__(self, name=None):
        self.name = name

class ArtefactosDetalles(db.Model):
    __tablename__ = 'Artefactos_Detalles'
    Artefactos_id = db.Column(db.Integer, db.ForeignKey('Artefactos.id'), primary_key=True)
    Detalles_id = db.Column(db.Integer, db.ForeignKey('Detalles.id'), primary_key=True)
    special_key = db.Column(db.String(50))

    # bidirectional attribute/collection of "Artefactos"/"Artefactos_Detalless"
    Artefactos = relationship(Artefactos, backref=backref("Artefactos_Detalles", cascade="all, delete-orphan"))
    # reference to the "Detalles" object
    Detalles = relationship("Detalles")
    # Reference to the "Detalles" column inside the "Detalles" object.
    Detalles_value = association_proxy('id', 'Detalles')

    def __init__(self, Detalles=None, Artefactos=None, special_key=None):
        self.Artefactos = Artefactos
        self.Detalles = Detalles
        self.special_key = special_key

class Detalles(db.Model):
    __tablename__ = 'Detalles'
    id = db.Column(db.Integer, primary_key=True)
    ancho_talon = db.Column(db.Integer)
    ancho_pieza = db.Column(db.Integer)
    long_pieza = db.Column(db.Integer)
    espesor_pieza = db.Column(db.Integer)
    tamanio_pieza = db.Column(db.Integer)
    mod_ancho_espesor_pieza = db.Column(db.Integer)
    clase_tecnica = db.Column(db.Enum(Clase_tecnica))
    reduc_uni_sbordes = db.Column(db.Enum(Reduc_uni_sbordes))
    las_inv_lim = db.Column(db.Enum(Las_inv_lim))
    proc_Tec = db.Column(db.Enum(Proc_Tec))
    forma_geo = db.Column(db.Enum(Forma_geo))
    angulo_bisel = db.Column(db.Integer)
    estado_bisel = db.Column(db.Enum(Estado_bisel))
    mantenimiento = db.Column(db.Enum(Mantenimiento))
    parte_pasiva =  db.Column(db.Enum(Parte_pasiva))
    ubicacion_pasiva = db.Column('Ubicacion parte pasiva', db.String(64))

    # Association proxy of "Detalles_Subgruposs" collection to "Subgrupos" attribute - a list of Subgruposs objects.
    Subgrupos = association_proxy('Detalles_Subgrupos', 'Subgrupos')
    # Association proxy to association proxy - a list of Subgruposs strings.
    Subgrupos_values = association_proxy('Detalles_Subgrupos', 'Subgrupos_value')

    def __init__(self, Detalles=None):
        self.Detalles = Detalles

    def __repr__(self):
        return 'Detalles(%s)' % repr(self.id)

class DetallesSubgrupos(db.Model):
    __tablename__ = 'Detalles_Subgrupos'
    Detalles_id = db.Column(db.Integer, db.ForeignKey('Detalles.id'), primary_key=True)
    Subgrupos_id = db.Column(db.Integer, db.ForeignKey('Subgrupos.id'), primary_key=True)
    special_key = db.Column(db.String(50))

    # bidirectional attribute/collection of "Detalles"/"Detalles_Subgruposs"
    Detalles = relationship(Detalles, backref=backref("Detalles_Subgrupos", cascade="all, delete-orphan"))
    # reference to the "Subgrupos" object
    Subgrupos = relationship("Subgrupos")
    # Reference to the "Subgrupos" column inside the "Subgrupos" object.
    Subgrupos_value = association_proxy('id', 'Subgrupos')

    def __init__(self, Subgrupos=None, Detalles=None, special_key=None):
        self.Detalles = Detalles
        self.Subgrupos = Subgrupos
        self.special_key = special_key

class Subgrupos(db.Model):
    __tablename__ = 'Subgrupos'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String)

    def __init__(self, Subgrupos=None):
        self.Subgrupos = Subgrupos

    def __repr__(self):
        return 'Subgrupos(%s)' % repr(self.id)

class Desechos(db.Model):
    __tablename__ = 'Desechos'
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column('Nombre o etiqueta de la observación', db.String(64))
    sitio = db.Column('Sitio o Localidad', db.String(64))
    sigla = db.Column('Sigla del sitio', db.String(64))
    capa = db.Column('Capa o nivel', db.String(64))
    cuadro = db.Column('Cuadro o microsector', db.String(64))
    coleccion = db.Column('Colección o año', db.String(64))
    nro_sintalon = db.Column('Piezas fracturadas sin talon', db.Enum(Cant_Filos))
    nro_contalon = db.Column('Piezas fracturadas con talon', db.Enum(Cant_Filos))
    lote = db.Column('Lote', db.Integer)
    estado = db.Column(db.Enum(Estado))
    ancho_talon = db.Column(db.Integer)
    ancho_pieza = db.Column(db.Integer)
    long_pieza = db.Column(db.Integer)
    espesor_pieza = db.Column(db.Integer)
    sup_talon = db.Column(db.Enum(Superficie_Talon))

    # Association proxy of "Desechos_Subgruposs" collection to "Subgrupos" attribute - a list of Subgruposs objects.
    Subgrupos = association_proxy('Desechos_Subgrupos', 'Subgrupos')
    # Association proxy to association proxy - a list of Subgruposs strings.
    Subgrupos_values = association_proxy('Desechos_Subgrupos', 'Subgrupos_value')

class DesechosSubgrupos(db.Model):
    __tablename__ = 'Desechos_Subgrupos'
    Desechos_id = db.Column(db.Integer, db.ForeignKey('Desechos.id'), primary_key=True)
    Subgrupos_id = db.Column(db.Integer, db.ForeignKey('Subgrupos.id'), primary_key=True)
    special_key = db.Column(db.String(50))

    # bidirectional attribute/collection of "Desechos"/"Desechos_Subgruposs"
    Desechos = relationship(Desechos, backref=backref("Desechos_Subgrupos", cascade="all, delete-orphan"))
    # reference to the "Subgrupos" object
    Subgrupos = relationship("Subgrupos")
    # Reference to the "Subgrupos" column inside the "Subgrupos" object.
    Subgrupos_value = association_proxy('name', 'Subgrupos')

    def __init__(self, Subgrupos=None, Desechos=None, special_key=None):
        self.Desechos = Desechos
        self.Subgrupos = Subgrupos
        self.special_key = special_key

class ObservacionAdmin(sqla.ModelView):
    column_labels = dict(nombre='Nombre o etiqueta de la observación', sitio = 'Sitio o Localidad',
    sigla = 'Sigla del Sitio', capa = 'Capa o nivel',
    coleccion = 'Colección', operador = 'Operador', fecha = 'Fecha',
    hoja = 'Hoja N°', Artefactos='Artefactos')

    column_list = ('id', 'nombre', 'sitio', 'sigla', 'capa', 'coleccion', 'operador',
    'fecha', 'hoja', 'Artefactos')

    can_export = True

class ArtefactosAdmin(sqla.ModelView):
    """ Flask-admin can not automatically find a association_proxy yet. You will
        need to manually define the column in list_view/filters/sorting/etc.
        Moreover, support for association proxies to association proxies
        (e.g.: Detalles_values) is currently limited to column_list only."""

    column_labels = dict(id = 'Id', numero = 'N°',
        cuadro = 'Cuadro o microsector', roca = 'Roca o materia prima',
        estado = 'Estado', tipo ="Tipo", eje = "Eje",
        clase_art = 'Clase Artefactual',
        cant_filos = 'N° de filos/ cicatrices',
        cant_puntas = 'N° de puntas formatizadas',
        clasificacion_Forma_Base = 'Tipo de forma base',
        cant_Cicatrices = 'N° cicatrices lascado',
        origen = 'Origen',
        estado_talon = 'Estado del talón',
        sup_talon = 'Sup talón o plataforma',
        ancho_talon = 'Ancho talón (mm)',
        ancho_pieza = 'Ancho pieza (mm)',
        long_pieza = 'Longitud pieza (mm)',
        espesor_pieza = 'Espesor pieza (mm)',
        peso_pieza = 'Peso pieza (g)',
        tamanio_pieza = 'Tamaño pieza',
        mod_ancho_largo_pieza = 'Módulo ancho-largo',
        mod_ancho_espesor_pieza = 'Módulo ancho-espesor',
        clase_tecnica = 'Clase técnica',
        reduc_uni_sbordes = "Reduccion unifacial sin bordes",
        las_inv_lim = "Lascado inverso limitante",
        proc_Tec = "Procedimiento técnico de formatización",
        forma_geo = "Forma geométrica",
        angulo_bisel = "Ángulo bisel estimado",
        estado_bisel = "Estado bisel",
        mantenimiento = "Mantenimiento",
        parte_pasiva = "Parte pasiva",
        forma_lascados = "Forma de los lascados de formatización",
        sustancia = "Sustancia adherida",
        ubicacion_sustancia = "Ubicacion de la sustancia adherida",
        obs = "Observaciones",
        Detalles = 'Detalles')

    column_list = ('id', 'numero', 'cuadro', 'roca', 'estado', 'tipo', 'eje',
    'clase_art', 'cant_filos', 'cant_puntas', 'clasificacion_Forma_Base',
    'cant_Cicatrices', 'origen', 'alteraciones', 'estado_talon',
    'sup_talon', 'ancho_talon', 'long_pieza', 'ancho_pieza', 'espesor_pieza',
    'peso_pieza', 'tamanio_pieza', 'mod_ancho_largo_pieza', 'mod_ancho_espesor_pieza',
    'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo',
    'angulo_bisel', 'estado_bisel', 'mantenimiento', 'parte_pasiva',
    'forma_lascados', 'sustancia', 'ubicacion_sustancia', 'obs','Detalles')

    column_sortable_list = ('id', 'numero', 'cuadro', 'roca', 'estado', 'tipo', 'eje',
    'clase_art', 'cant_filos', 'cant_puntas', 'clasificacion_Forma_Base',
    'cant_Cicatrices', 'origen', 'alteraciones', 'estado_talon',
    'sup_talon', 'ancho_talon', 'long_pieza', 'ancho_pieza', 'espesor_pieza',
    'peso_pieza', 'tamanio_pieza', 'mod_ancho_largo_pieza', 'mod_ancho_espesor_pieza',
    'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo',
    'angulo_bisel', 'estado_bisel', 'mantenimiento','parte_pasiva',
    'forma_lascados', 'sustancia', 'ubicacion_sustancia', 'obs','Detalles')

    column_filters = ('id', 'numero', 'cuadro', 'roca', 'estado', 'tipo', 'eje',
    'clase_art', 'cant_filos', 'cant_puntas', 'clasificacion_Forma_Base',
    'cant_Cicatrices', 'origen', 'alteraciones', 'estado_talon',
    'sup_talon', 'ancho_talon', 'long_pieza', 'ancho_pieza', 'espesor_pieza',
    'peso_pieza', 'tamanio_pieza', 'mod_ancho_largo_pieza', 'mod_ancho_espesor_pieza',
    'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo',
    'angulo_bisel', 'estado_bisel', 'mantenimiento','parte_pasiva',
    'forma_lascados', 'sustancia', 'ubicacion_sustancia', 'obs','Detalles')

    form_columns = ('id', 'numero', 'cuadro', 'roca', 'estado', 'tipo', 'eje',
    'clase_art', 'cant_filos', 'cant_puntas', 'clasificacion_Forma_Base',
    'cant_Cicatrices', 'origen', 'alteraciones', 'estado_talon',
    'sup_talon', 'ancho_talon', 'long_pieza', 'ancho_pieza', 'espesor_pieza',
    'peso_pieza', 'tamanio_pieza',
    'mod_ancho_largo_pieza', 'mod_ancho_espesor_pieza',
    'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo',
    'angulo_bisel', 'estado_bisel', 'mantenimiento', 'parte_pasiva',
    'forma_lascados', 'sustancia', 'ubicacion_sustancia', 'obs','Detalles')

    can_export = True


class DetallesAdmin(sqla.ModelView):

    column_labels = dict(id = 'Id', numero = 'N°',
        ancho_talon = 'Ancho del talón',
        long_pieza = 'Longitud pieza (mm)',
        ancho_pieza = 'Ancho pieza (mm)',
        espesor_pieza = 'Espesor pieza (mm)',
        tamanio_pieza = 'Tamaño de la pieza',
        mod_ancho_espesor_pieza = 'Módulo ancho-espesor',
        clase_tecnica = 'Clase técnica',
        reduc_uni_sbordes = "Reduccion unifacial sin bordes",
        las_inv_lim = "Lascado inverso limitante",
        proc_Tec = "Procedimiento técnico de formatización",
        forma_geo = "Forma geométrica",
        angulo_bisel = "Ángulo bisel estimado",
        estado_bisel = "Estado bisel",
        mantenimiento = "Mantenimiento",
        parte_pasiva = "Parte pasiva",
        ubicacion_pasiva = "Ubicación parte pasiva")

    column_list = ('id', 'long_pieza', 'ancho_pieza', 'espesor_pieza', 'tamanio_pieza', 'mod_ancho_espesor_pieza', 'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo', 'angulo_bisel', 'estado_bisel', 'mantenimiento','parte_pasiva', 'ubicacion_pasiva')
    column_sortable_list = ('id', 'long_pieza', 'ancho_pieza', 'espesor_pieza', 'tamanio_pieza','mod_ancho_espesor_pieza', 'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo', 'angulo_bisel', 'estado_bisel', 'mantenimiento', 'parte_pasiva', 'ubicacion_pasiva')
    column_filters = ('id', 'long_pieza', 'ancho_pieza', 'espesor_pieza', 'tamanio_pieza','mod_ancho_espesor_pieza', 'clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo', 'angulo_bisel', 'estado_bisel', 'mantenimiento', 'parte_pasiva', 'ubicacion_pasiva')
    form_columns = ('id', 'long_pieza', 'ancho_pieza', 'espesor_pieza', 'tamanio_pieza', 'mod_ancho_espesor_pieza','clase_tecnica', 'reduc_uni_sbordes', 'las_inv_lim', 'proc_Tec', 'forma_geo', 'angulo_bisel', 'estado_bisel', 'mantenimiento', 'parte_pasiva', 'ubicacion_pasiva')

    can_export = True

class DesechosAdmin(sqla.ModelView):
    column_labels = dict(nombre='Nombre o etiqueta de la observación', sitio = 'Sitio o Localidad',
    sigla = 'Sigla del Sitio', capa = 'Capa o nivel', cuadro = 'Cuadro o microsector',
    coleccion = 'Colección', nro_sintalon = 'N° Piezas fracturadas sin talon',
    nro_contalon = 'N° Piezas fracturadas con talon',
    lote = 'Lote',
    estado = "Estado",
    ancho_talon = "Ancho del talón",
    ancho_pieza = "Ancho de la pieza",
    long_pieza = "Longitud de la pieza",
    espesor_pieza = "Espesor de la pieza",
    sup_talon = "Superficie talon")

    column_list = ('id', 'nombre', 'sitio', 'sigla', 'capa', 'coleccion', 'nro_contalon', 'lote', 'estado', 'ancho_talon', 'ancho_pieza', 'long_pieza', 'espesor_pieza')
    column_sortable_list = ('id', 'nombre', 'sitio', 'sigla', 'capa', 'coleccion', 'nro_contalon', 'nro_sintalon', 'lote','estado', 'ancho_talon', 'ancho_pieza', 'long_pieza', 'espesor_pieza', 'sup_talon')
    column_filters = ('id', 'nombre', 'sitio', 'sigla', 'capa', 'coleccion', 'nro_contalon', 'nro_sintalon', 'lote','estado', 'ancho_talon', 'ancho_pieza', 'long_pieza', 'espesor_pieza', 'sup_talon')

    can_export = True

class SubgruposAdmin(sqla.ModelView):
    column_labels = dict(nombre='Nombre o etiqueta del subgrupo')

    column_list = ('id', 'nombre')
    column_sortable_list = ('id', 'nombre')
    column_filters = ('id', 'nombre')

    can_export = True


class CustomView(sqla.ModelView):
    chart = 'chart.html'

# Create admin
admin = admin.Admin(app, name='ARQcheros', template_mode='bootstrap3')
admin.add_view(ObservacionAdmin(Observacion, db.session))
admin.add_view(ArtefactosAdmin(Artefactos, db.session))
admin.add_view(DetallesAdmin(Detalles, db.session))
admin.add_view(DesechosAdmin(Desechos, db.session))
admin.add_view(SubgruposAdmin(Subgrupos, db.session))
#admin.add_view(CustomView(Artefactos, db.session))

if __name__ == '__main__':

    # Create DB
    db.create_all()

    # Add sample data
    Artefactos = Artefactos()

    db.session.add(Artefactos)
    db.session.commit()

    # Start app
    app.run(debug=True)
